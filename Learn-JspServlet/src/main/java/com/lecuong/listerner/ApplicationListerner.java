package com.lecuong.listerner;

import com.lecuong.beans.BeanFactory;
import com.lecuong.cache.Cache;
import com.lecuong.cache.CacheImpl;
import com.lecuong.entity.User;
import com.lecuong.model.response.user.UserResponse;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ApplicationListerner implements ServletContextListener {

    public static Cache<String, UserResponse> userCache = null;

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        this.userCache = new CacheImpl<>();
        BeanFactory beanFactory = new BeanFactory();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

    }
}
