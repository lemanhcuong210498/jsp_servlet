package com.lecuong.util;

import com.lecuong.annotation.Column;
import com.lecuong.annotation.Entity;
import com.lecuong.annotation.Id;

public class AnnotationUtil {

    public static String getTableName(Class<?> tClass) {
        return tClass.getAnnotation(Entity.class).name();
    }

    public static String getColumnName(Class<?> tClass, String fieldName) throws NoSuchFieldException {
        return tClass.getDeclaredField(fieldName).getAnnotation(Column.class).name();
    }

    public static String getPrimaryKey(Class<?> tClass, String fieldName) throws NoSuchFieldException {
        return tClass.getDeclaredField(fieldName).getAnnotation(Id.class).name();
    }
}
