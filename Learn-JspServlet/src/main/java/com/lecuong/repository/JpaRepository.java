package com.lecuong.repository;

import com.lecuong.paging.PageAble;

import java.io.Serializable;
import java.util.Optional;
import java.util.stream.Stream;

public interface JpaRepository<T, ID extends Serializable> {

    T insert(T t) throws NoSuchFieldException;

    T update(ID id, T t) throws NoSuchFieldException;

    void delete(ID id);

    Optional<T> findById(ID id);

    Stream<T> findAll();

    Stream<T> findAll(PageAble pageAble);

    long count();
}
