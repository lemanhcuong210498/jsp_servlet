package com.lecuong.repository;

import com.lecuong.entity.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findUserByUserNameAndPassword(String userName, String password);


}
