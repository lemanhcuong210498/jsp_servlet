package com.lecuong.repository.impl;

import com.lecuong.annotation.Repository;
import com.lecuong.entity.Role;
import com.lecuong.repository.RoleRepository;
import com.lecuong.util.ReflectionUtil;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class RoleRepositoryImpl extends BaseQuery<Role, Long> implements RoleRepository {

    @Override
    public List<Role> findRoleById(long userId) {

        String sql = "SELECT r.* FROM role r JOIN permission p ON r.id = p.role_id WHERE p.user_id = ?";

        List<Role> listRole = new ArrayList<>();

        try {
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setLong(1, userId);
            ResultSet rs = ps.executeQuery();

            while (rs.next()){
                Role role = ReflectionUtil.convertToEntity(rs, Role.class);
                listRole.add(role);
            }
        } catch (SQLException | IllegalAccessException | InstantiationException | NoSuchFieldException throwables) {
            throwables.printStackTrace();
        }

        return listRole;
    }
}
