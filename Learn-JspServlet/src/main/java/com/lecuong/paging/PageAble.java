package com.lecuong.paging;

public interface PageAble {

    int getOffSet();

    int getSize();

    int getPageIndex();
}
