package com.lecuong.controller;

import com.lecuong.annotation.Autowire;
import com.lecuong.beans.BeanFactory;
import com.lecuong.model.response.BaseResponse;
import com.lecuong.model.response.user.UserResponse;
import com.lecuong.paging.PageRequest;
import com.lecuong.service.UserService;
import com.lecuong.util.BeanUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/admin")
public class AdminController extends HttpServlet {

    @Autowire
    private UserService userService;

    public AdminController() {
        this.userService = (UserService) BeanFactory.beans.get("userService");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        PageRequest pageRequest = PageRequest.of(1, 10);
        BaseResponse<UserResponse> users = userService.getAll(pageRequest);

        req.setAttribute("users", users);

        RequestDispatcher requestDispatcher = req.getRequestDispatcher("/views/admin.jsp");
        requestDispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
