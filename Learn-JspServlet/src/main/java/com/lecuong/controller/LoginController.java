package com.lecuong.controller;

import com.lecuong.annotation.Autowire;
import com.lecuong.annotation.Controller;
import com.lecuong.beans.BeanFactory;
import com.lecuong.model.request.user.AuthRequest;
import com.lecuong.model.response.user.UserResponse;
import com.lecuong.security.SecurityContext;
import com.lecuong.service.UserService;
import com.lecuong.util.BeanUtil;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;

@WebServlet("/login")
@Controller
public class LoginController extends HttpServlet {

    private final SecurityContext securityContext;

    public LoginController() {
        securityContext = (SecurityContext) BeanFactory.beans.get("securityContext");
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        RequestDispatcher rs = req.getRequestDispatcher("views/authen/login.jsp");
        rs.forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

//        try {
//            AuthRequest authRequest = BeanUtil.toModel(AuthRequest.class, req);
//            System.out.println(authRequest);
//
//            UserResponse userResponse = userService.auth(authRequest);
//
//            if (userResponse != null){
//                resp.sendRedirect("/home");
//            }
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (NoSuchMethodException e) {
//            e.printStackTrace();
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        } catch (InvocationTargetException e) {
//            e.printStackTrace();
//        }

        AuthRequest authRequest = BeanUtil.toModel(AuthRequest.class, req);
        String url = securityContext.authorization(authRequest, req);

        resp.sendRedirect(url);
    }
}
