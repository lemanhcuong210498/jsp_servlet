package com.lecuong.security;

import com.lecuong.model.request.user.AuthRequest;

import javax.servlet.http.HttpServletRequest;

public interface SecurityContext {

    String authorization(AuthRequest authRequest, HttpServletRequest request);

}
