package com.lecuong.security;

import com.lecuong.annotation.Autowire;
import com.lecuong.annotation.Component;
import com.lecuong.common.ConsantKey;
import com.lecuong.entity.Role;
import com.lecuong.model.request.user.AuthRequest;
import com.lecuong.model.response.role.RoleResponse;
import com.lecuong.model.response.user.UserResponse;
import com.lecuong.service.RoleService;
import com.lecuong.service.UserService;
import com.lecuong.util.RandomUtil;
import com.lecuong.util.SessionUtil;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class SecurityContextImpl implements SecurityContext {

    @Autowire
    private final UserService userService;

    @Autowire
    private final RoleService roleService;

    public SecurityContextImpl(UserService userService, RoleService roleService) {
        this.userService = userService;
        this.roleService = roleService;
    }


    @Override
    public String authorization(AuthRequest authRequest, HttpServletRequest request) {

        try {
            UserResponse user = userService.auth(authRequest);
            if (user != null){
                SessionUtil.put(request, ConsantKey.SESSION_KEY, user.getUserName());
                List<String> roles = roleService
                        .findAllRoleByUserId(user.getId())
                        .stream()
                        .map(role -> role.getName())
                        .collect(Collectors.toList());

                if (roles.contains("ADMIN")){
                    return "/admin";
                }else {
                    return "/home";
                }
            }
            return "/login?message=Tai khoan hoac mat khau khong ton tai";
        } catch (NoSuchAlgorithmException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return "";
        }
    }
}
