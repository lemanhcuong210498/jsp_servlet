package com.lecuong.entity;

import com.lecuong.annotation.Column;
import com.lecuong.annotation.Entity;
import com.lecuong.annotation.Id;

@Entity(name = "permission")
public class Permission {

    @Column(name = "user_id")
    private long userId;

    @Column(name = "role_id")
    private long roleId;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }
}
