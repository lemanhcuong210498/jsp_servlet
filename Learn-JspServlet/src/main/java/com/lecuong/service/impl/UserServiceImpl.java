package com.lecuong.service.impl;

import com.lecuong.annotation.Autowire;
import com.lecuong.annotation.Service;
import com.lecuong.entity.User;
import com.lecuong.exception.ObjectNotFoundException;
import com.lecuong.exception.ValidateException;
import com.lecuong.listerner.ApplicationListerner;
import com.lecuong.mapper.UserMapper;
import com.lecuong.model.request.user.AuthRequest;
import com.lecuong.model.response.BaseResponse;
import com.lecuong.model.response.user.UserResponse;
import com.lecuong.paging.PageAble;
import com.lecuong.repository.UserRepository;
import com.lecuong.service.UserService;
import com.lecuong.util.PasswordHasher;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.lecuong.mapper.UserMapper.*;

@Service
public class UserServiceImpl implements UserService {

    @Autowire
    private UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
        Stream<User> userStream = userRepository.findAll();
        userStream.forEach(user -> {
            ApplicationListerner.userCache.put(user.getUserName(), mapToResponse(user));
        });
    }

    @Override
    public UserResponse auth(AuthRequest authRequest) throws NoSuchAlgorithmException, NoSuchMethodException, IllegalAccessException, InvocationTargetException {

        //isBlank = isEmpty() && null
        if (StringUtils.isBlank(authRequest.getUserName())) {
            throw new ValidateException("UserName is not blank");
        }
        if (StringUtils.isBlank(authRequest.getPassword())) {
            throw new ValidateException("Password is not blank");
        }

        UserResponse userResponse = ApplicationListerner.userCache.get(authRequest.getUserName());

        String password = PasswordHasher.hash(authRequest.getPassword());

        if (userResponse == null) {
            Optional<User> user = userRepository.findUserByUserNameAndPassword(authRequest.getUserName(), password);

            user.orElseThrow(() -> new ObjectNotFoundException("User not found"));

            UserResponse userResponse1 = mapToResponse(user.get());

            ApplicationListerner.userCache.put(userResponse.getUserName(), userResponse);

            return userResponse;
        }

        if (userResponse.getPassword().equals(password)) {
            return userResponse;
        }

        return null;

//        if (user == null){
//            throw new ObjectNotFoundException("User not found");
//        }else{
//            return user;
//        }
    }

    @Override
    public BaseResponse<UserResponse> getAll(PageAble pageAble) {

        List<User> users = userRepository.findAll(pageAble).collect(Collectors.toList());
        List<UserResponse> data = users.stream().map(UserMapper::mapToResponse).collect(Collectors.toList());

        long total = userRepository.count();

        return BaseResponse.of(total, data);
    }
}
