package com.lecuong.service;

import com.lecuong.entity.User;
import com.lecuong.model.request.user.AuthRequest;
import com.lecuong.model.response.BaseResponse;
import com.lecuong.model.response.user.UserResponse;
import com.lecuong.paging.PageAble;

import java.lang.reflect.InvocationTargetException;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

public interface UserService {

    UserResponse auth(AuthRequest authRequest) throws NoSuchAlgorithmException, NoSuchMethodException, IllegalAccessException, InvocationTargetException;

    BaseResponse<UserResponse> getAll(PageAble pageAble);
}
